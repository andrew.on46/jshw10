const input1 = document.getElementsByTagName("input")[0];
const input2 = document.getElementsByTagName("input")[1];

const icon1 = document.getElementsByTagName("i")[0];
const icon2 = document.getElementsByTagName("i")[1];

const btn = document.querySelector(".btn");

icon1.onclick = () => {
  if (icon1.classList.contains("fa-eye")) {
    icon1.classList.replace("fa-eye", "fa-eye-slash");
    input1.type = "text";
  } else {
    icon1.classList.replace("fa-eye-slash", "fa-eye");
    input1.type = "password";
  }
};

icon2.onclick = () => {
  if (icon2.classList.contains("fa-eye")) {
    icon2.classList.replace("fa-eye", "fa-eye-slash");
    input2.type = "text";
  } else {
    icon2.classList.replace("fa-eye-slash", "fa-eye");
    input2.type = "password";
  }
};
btn.onclick = () => {
  if (input1.value === input2.value) {
    alert("You are welcome");
  } else {
    alert("Нужно ввести одинаковые значения");
  }
};
